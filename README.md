# Master RESO

Analyse spatiale de réseaux : novembre 2024

- Séance 1 – 18/11, 14:00-17:00 : [les lieux qui font liens](https://framagit.org/nice/reso/-/tree/seance-1)
- Séance 2 – 21/11, 14:00-17:00 : [représentation graphique de réseaux](https://framagit.org/nice/reso/-/tree/seance-2) 
- Séance 3 - 25/11, 14:00-17:00 : [le commerce international](https://framagit.org/nice/reso/-/tree/seance-3)
- Séance 4 – 27/11, 14:00-17:00 : [les réseaux scientifiques](https://framagit.org/nice/reso/-/tree/seance-4)
